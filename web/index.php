<?php

require '../vendor/autoload.php';
require_once __DIR__ . '/../app/autoload.php';
require_once __DIR__ . '/../app/AppBootstrap.php';
require_once __DIR__ . '/../app/AppKernel.php';

use Symfony\Component\HttpFoundation\Request;

$container = (include '../dependency.php');

$request = Request::createFromGlobals();
$bootstrap = new Bootstrap('prod', $container);

$kernel = new AppKernel($request, $bootstrap->container);
$kernel->handle();
