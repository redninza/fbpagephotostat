<?php
/**
 * Define all the common dependencies here and return as a Pimple container
 * @see https://github.com/fabpot/Pimple
 */

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Doctrine\DBAL\Configuration as DBALConfig,
    Doctrine\DBAL\DriverManager as DBALDriverManager,
    Doctrine\ORM\EntityManager,
    Doctrine\ORM\Configuration as ORMConfiguration;

$container = new Pimple();
$config = Yaml::parse(file_get_contents(__DIR__ . '/app/config/config.yml'));
$parameter = Yaml::parse(file_get_contents(__DIR__ . '/app/config/parameters.yml'));
$container['config'] = array_merge($config, $parameter);

$container['routes'] = loadRoutes();
$container['dbal'] = loadDBAL();
$container['em'] = loadORM();

return $container;

function loadRoutes()
{
    $route = array();
    $yaml = new Parser();
    $fileIterator = new DirectoryIterator(__DIR__ . '/app/config/routes');

    foreach ($fileIterator as $file) {
        if ($file->isFile() && $file->getExtension() == 'yml') {
            $routes = $yaml->parse(file_get_contents(__DIR__ . '/app/config/routes/' . $file->getFilename()));
            $route = array_merge($route, $routes);
        }
    }

    return $route;
}

function loadDBAL()
{
    return function($c){

        $dbalConfig = new \Doctrine\DBAL\Configuration();

        $db   = $c['config']['mysql']['db'];
        $host = $c['config']['mysql']['host'];
        $user = $c['config']['mysql']['user'];
        $pass = $c['config']['mysql']['pass'];

        $connectionParams = array(
            'dbname'   => $db,
            'user'     => $user,
            'password' => $pass,
            'host'     => $host,
            'driver'   => 'pdo_mysql',
        );

        $conn = DBALDriverManager::getConnection($connectionParams, $dbalConfig);
        return $conn;

    };
}

function loadORM()
{
     return function($c){

        $cache = new \Doctrine\Common\Cache\ArrayCache;

        $config = new ORMConfiguration;
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        $driverImpl = $config->newDefaultAnnotationDriver(__DIR__ . '/src/Entities');
        $config->setMetadataDriverImpl($driverImpl);

        $config->setProxyDir(__DIR__ . '/app/cache/Proxies');
        $config->setProxyNamespace('Proxies');

        $config->setAutoGenerateProxyClasses(true);

        $em = EntityManager::create($c['dbal'], $config);
        return $em;

    };
}