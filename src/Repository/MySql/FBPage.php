<?php

namespace Repository\MySql;

use Doctrine\ORM\EntityRepository;

use Entities\FBPage as FBPageEntity;
use Mapper\FBPage as FBPageMapper;

class FBPage extends EntityRepository
{

    /**
     * @var FBPageEntity
     */
    protected $fbpageEntity;

    /**
     * @var \Pimple
     */
    protected $container;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @param \Pimple $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $container
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * @param Array $data to insert
     * @return array inserted message array
     */
    public function insert($data)
    {
        $fbpageEntity = $this->getByPhotoId($data['photo_id']);

        if (!($fbpageEntity instanceof FBPageEntity)) {
            $fbpageEntity = new FBPageEntity();
        }

        $fbpageMapper = new FBPageMapper();
        $mapped = $fbpageMapper->map($data, $fbpageEntity);

        if ($mapped->isValid() === false) {
            throw new \InvalidArgumentException("Wrong data provided");
        }

        $this->_em->persist($mapped);
        $this->_em->flush();

        return $mapped->toArray();
    }

    /**
     * @param $photoId
     * @return bool|null|object
     */
    public function getByPhotoId($photoId)
    {
        $photoEntity = $this->findOneBy(array('photoId' => $photoId));

        if (!$photoEntity) {
            return false;
        }

        return $photoEntity;
    }

    /**
     * @param $id
     * @return bool|null|object
     */
    public function getById($id)
    {
        $photoEntity = $this->find($id);

        if (!$photoEntity) {
            return array();
        }

        return $photoEntity->toArray();
    }

    /**
     * Crawl FB Array
     * @return array
     */
    public function getPhotosFromFB()
    {
        $fbResponse = $this->getApiResponse();
        $decoded = json_decode($fbResponse);
        $data = $decoded->data;

        $photos = array();
        foreach($data as $photo) {
            $formatted = $this->getFormattedData($photo);
            $photos[] = $this->insert($formatted);
        }

        return $photos;
    }

    private function getApiResponse()
    {
        $apiBase  = $this->container['config']['api_endpoint'];
        $pageName = $this->container['config']['page']['name'];

        $apiEndpoint = str_replace('{pageid}', $pageName, $apiBase);
        $response = file_get_contents($apiEndpoint);

        return $response;
    }

    private function getFormattedData($photo)
    {
        $formatted = array();
        $formatted['photo_id']        = $photo->id;
        $formatted['name']            = isset($photo->name) ? $photo->name : '';
        $formatted['image_url']       = $photo->source;
        $formatted['created']         = $photo->created_time;
        $formatted['number_of_likes'] = isset($photo->likes) ? count($photo->likes->data) : 0;

        return $formatted;
    }

    public function getPhotoList()
    {
        $fbPhotos = array();
        $photos = $this->findAll();

        if (!empty($photos)) {
            foreach($photos as $photo) {
                $fbPhotos[] = $photo->toArray();
            }
        }

        return $fbPhotos;
    }
}