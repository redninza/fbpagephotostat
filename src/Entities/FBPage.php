<?php

namespace Entities;

use Respect\Validation\Validator;

/**
 * @Entity(repositoryClass="Repository\MySql\FBPage")
 * @Table(name="fbpage")
 */
class FBPage
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(length=200)
     */
    protected $photoId;

    /**
     * @Column(length=100)
     */
    protected $name;

    /**
     * @Column(length=400)
     */
    protected $imageUrl;

    /**
     * @Column(type="datetime", nullable=true)
     */
    protected $created;

    /**
     * @Column(type="integer")
     */
    protected $numberOfLikes;

    public function toArray()
    {
        $data = array(
            'id'                => $this->getId(),
            'photo_id'          => $this->getPhotoId(),
            'name'              => $this->getName(),
            'image_url'         => $this->getImageUrl(),
            'created'           => $this->getCreated(),
            'number_of_likes'   => $this->getNumberOfLikes()
        );

        return $data;
    }

    public function isValid()
    {
        try {

            Validator::create()->notEmpty()->assert($this->getPhotoId());
            Validator::create()->notEmpty()->assert($this->getImageUrl());

        } catch (\InvalidArgumentException $e) {

            return false;
        }

        return true;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return mixed
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $numberOfLikes
     */
    public function setNumberOfLikes($numberOfLikes)
    {
        $this->numberOfLikes = $numberOfLikes;
    }

    /**
     * @return mixed
     */
    public function getNumberOfLikes()
    {
        return $this->numberOfLikes;
    }

    /**
     * @param mixed $photo_id
     */
    public function setPhotoId($photo_id)
    {
        $this->photoId = $photo_id;
    }

    /**
     * @return mixed
     */
    public function getPhotoId()
    {
        return $this->photoId;
    }
}