<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Response;
use Repository\MySql\FBPage as FBPageRepository;

class FBPage extends Base
{
    /** @var FBPageRepository */
    protected $fbpageRepository;

    public function init()
    {
        $this->response = new Response();
        $this->response->headers->set('Content-Type', 'application/json');

        //if ($this->config['persister_choice'] == 'mysql') {
        $this->fbpageRepository = $this->em->getRepository('Entities\FBPage');
        //}

        $this->fbpageRepository->setContainer($this->container);
        $this->fbpageRepository->setRequest($this->request);
    }

    /**
     * GET /photos/crawl
     * Crawling FB with Graph API and stor photo info at DB
     */
    public function getPhotosFromFB()
    {
        try {
            $response = $this->fbpageRepository->getPhotosFromFB();

            $message = $this->twig->loadTemplate('imported.html.twig');
            echo $message->render(array());exit;

        } catch (\Exception $e) {

            $this->response->setContent(json_encode(array('result' => $e->getMessage())));
            $this->response->setStatusCode($e->getCode());
        }

        return $this->response;
    }

    /**
     * GET /
     * GET /photos/list
     * Showing list of photos previously crawled and stored in DB
     */
    public function showPhotoList()
    {
        try {
            $response = $this->fbpageRepository->getPhotoList();

            $list = $this->twig->loadTemplate('photolist.html.twig');
            echo $list->render(array('data' => $response, 'page' => strtoupper($this->config['page']['name'])));exit;

        } catch (\Exception $e) {

            $this->response->setContent(json_encode(array('result' => $e->getMessage())));
            //$this->response->setStatusCode($e->getCode() ? $e->getCode() : 400);
        }

        return $this->response;
    }

    /**
     * GET /photos/{id}
     * @param $id
     * Fetch details of a Photo
     */
    public function showPhotoDetails($id)
    {
        try {
            $response = $this->fbpageRepository->getById($id);

            $photo = $this->twig->loadTemplate('photo.html.twig');
            echo $photo->render(array('data' => $response, 'page' => $this->config['page']['name']));exit;

        } catch (\Exception $e) {

            $this->response->setContent(json_encode(array('result' => $e->getMessage())));
            $this->response->setStatusCode($e->getCode());
        }

        return $this->response;
    }
}