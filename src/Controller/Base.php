<?php

namespace Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class Base
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Response;
     */
    protected $response;

    /**
     * @var array
     */
    protected $config;

    /**
     * @var \Pimple
     */
    protected $container;

    /** @var \Twig_Environment */
    protected $twig;

    public function setContainer($container)
    {
        $this->container = $container;

        $this->em     = $container['em'];
        $this->config = $container['config'];
        $this->twig   = $container['twig'];
    }

    /**
     * Inject the Request object for further use.
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    /**
     * Initializer function to be used by child classes.
     */
    public function init(){}
}