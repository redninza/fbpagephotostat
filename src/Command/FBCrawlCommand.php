<?php

namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Repository\MySql\FBPage;

class FBCrawlCommand extends Command
{
    /**
     * @var \Pimple
     */
    protected $container;

    public function __construct(\Pimple $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('fb:crawl');
        $this->setDescription('Crawl FB graph API for getting photo information');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("FB crawling starts ... ... ...");

        $repository = $this->container['em']->getRepository('Entities\FBPage');
        $repository->setContainer($this->container);
        $photos = $repository->getPhotosFromFB();

        $output->writeln("FB crawling ends!");
        $output->writeln(count($photos) . " photos inserted/updated!");
    }
}