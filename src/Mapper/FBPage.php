<?php

namespace Mapper;

use Entities\FBPage as FBPageEntity;

class FBPage
{

    public function map($data, FBPageEntity $fbpageEntity)
    {
        if (!empty($data['photo_id'])){
            $fbpageEntity->setPhotoId($data['photo_id']);
        }

        if (!empty($data['name'])){
            $fbpageEntity->setName($data['name']);
        }

        if (!empty($data['image_url'])){
            $fbpageEntity->setImageUrl($data['image_url']);
        }

        if (!empty($data['number_of_likes'])){
            $fbpageEntity->setNumberOfLikes($data['number_of_likes']);
        }

        $fbpageEntity->setCreated(new \DateTime());

        return $fbpageEntity;
    }
}