Facebook page uploaded photo statistics 
===================================================

#### This solution is developed on top of Lemon (https://github.com/phpfour/Lemon).
_________________________________________________________
## Configure:

* I have used ``composer`` packages in this app. In order to install the packages pls run the following command
```
php composer.phar install
```

* I have used ``twig`` template for front end. In order to run twig template - we need to give 0777 permission to the ``src/templates/cache`` folder.

* Before running the app the database needs to be configured. Please execute the ``messbari.sql`` file from the``Schema`` folder.

* Replace the values of  ``app/config/config.yml`` file mysql credentials with your own settings.

* If you want to grab photos from a page other than ``messbari`` - you can change the name of the page at ``app/config/config.yml`` file.


## Use:

* Getting information from FB page can be run from both browser and command line.
** From command line the command is:
```
php app.php fb:crawl
```
** I have created a virtual host pointing to the ``web/index.php`` to run the app from browser with the following settings:
```
<VirtualHost 127.0.0.9:80>
    ServerName   fb.messbari.local
    DocumentRoot /var/www/messbarifb/web

    RewriteEngine off

    <Location />
        RewriteEngine On
        RewriteCond %{REQUEST_FILENAME} -s [OR]
        RewriteCond %{REQUEST_FILENAME} -l [OR]
        RewriteCond %{REQUEST_FILENAME} -d
        RewriteRule ^.*$ - [NC,L]
        RewriteRule ^.*$ /index.php [NC,L]
    </Location>
</VirtualHost>
```
