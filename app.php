#!/usr/bin/env php
<?php

require_once 'vendor/autoload.php';
require_once 'app/autoload.php';
require_once 'src/Command/FBCrawlCommand.php';

use Symfony\Component\Console\Application;

$container = (include 'dependency.php');

$fbCrawler = new Application('Messbari Facebook API task', '1.0');
$fbCrawler->add(new Command\FBCrawlCommand($container));
$fbCrawler->run();
