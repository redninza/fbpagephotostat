<?php

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Parser;
use Doctrine\Common\Annotations\AnnotationReader,
    Doctrine\DBAL\Configuration as DBALConfig,
    Doctrine\DBAL\DriverManager as DBALDriverManager,
    Doctrine\ORM\EntityManager,
    Doctrine\ORM\Configuration as ORMConfiguration;

class Bootstrap
{
    /**
     * @var array
     */
    public $routes = array();

    /**
     * @var Pimple
     */
    public $container;

    public function __construct($env, Pimple $container)
    {
        $this->container = $container;
        $this->loadRoutes();
        $this->loadConfig();
        $this->loadDBAL();
        $this->loadORM();
        $this->loadTwig();
        $this->loadMD();
    }

    private function loadRoutes()
    {
        $yaml = new Parser();
        $fileIterator = new DirectoryIterator(__DIR__ . '/config/routes');

        foreach ($fileIterator as $file) {
            if ($file->isFile() && $file->getExtension() == 'yml') {
                $routes = $yaml->parse(file_get_contents(__DIR__ . '/config/routes/' . $file->getFilename()));
                $this->routes = array_merge($this->routes, $routes);
            }
        }

        $this->container['routes'] = $this->routes;
    }

    private function loadConfig()
    {
        $config = Yaml::parse(file_get_contents(__DIR__ . '/config/config.yml'));
        $parameter = Yaml::parse(file_get_contents(__DIR__ . '/config/parameters.yml'));
        $container['config'] = array_merge($config, $parameter);
    }

    private function loadDBAL()
    {
        $this->container['dbal'] = $this->container->factory(function($c){

            $dbalConfig = new \Doctrine\DBAL\Configuration();

            $db   = $c['config']['mysql']['db'];
            $host = $c['config']['mysql']['host'];
            $user = $c['config']['mysql']['user'];
            $pass = $c['config']['mysql']['pass'];

            $connectionParams = array(
                'dbname'   => $db,
                'user'     => $user,
                'password' => $pass,
                'host'     => $host,
                'driver'   => 'pdo_mysql',
            );

            $conn = DBALDriverManager::getConnection($connectionParams, $dbalConfig);
            return $conn;

        });
    }

    private function loadORM()
    {
        $this->container['em'] = $this->container->factory(function($c){

        $cache = new \Doctrine\Common\Cache\ArrayCache;

        $config = new ORMConfiguration;
        $config->setMetadataCacheImpl($cache);
        $config->setQueryCacheImpl($cache);

        $driverImpl = $config->newDefaultAnnotationDriver(__DIR__ . '/../src/Entities');
        $config->setMetadataDriverImpl($driverImpl);

        $config->setProxyDir(__DIR__ . '/cache/Proxies');
        $config->setProxyNamespace('Proxies');

        $config->setAutoGenerateProxyClasses(true);

        $em = EntityManager::create($c['dbal'], $config);
        return $em;

        });
    }

    private function loadTwig()
    {
        $twigLoader = new Twig_Loader_Filesystem(__DIR__ . '/../src/templates');
        $this->container['twig'] = new Twig_Environment($twigLoader, array('cache' => __DIR__ . '/../src/templates/cache'));
    }

    private function loadMD()
    {
        $this->container['md'] = new Mobile_Detect();

        /*$deviceView = new SunCat\MobileDetectBundle\Helper\DeviceView($this->container);
        $this->container['twig']->addExtension(new SunCat\MobileDetectBundle\Twig\Extension\MobileDetectExtension($this->container['md'], $deviceView));*/
    }
}