<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\ClassLoader\UniversalClassLoader;

$loader = new UniversalClassLoader();
$loader->register();

$src = array(
    'Command'          => __DIR__ . '/../src',
    'Controller'       => __DIR__ . '/../src',
    'Entities'         => __DIR__ . '/../src',
    'Mapper'           => __DIR__ . '/../src',
    'Repository\MySql' => __DIR__ . '/../src',
);

$loader->registerNamespaces($src);

